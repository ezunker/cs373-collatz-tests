#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  "")
        self.assertEqual(j, "")
    
    def test_read_3(self):
        s = "-1 -1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  -1)
        self.assertEqual(j, -1000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 100000)
        self.assertEqual(v, 351)

    def test_eval_2(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_3(self):
        v = collatz_eval(500, 100000)
        self.assertEqual(v, 351)

    def test_eval_4(self):
        v = collatz_eval(1, 200000)
        self.assertEqual(v, 383)

    def test_eval_5(self):
        v = collatz_eval(1, 200500)
        self.assertEqual(v, 383)
    
    def test_eval_6(self):
        v = collatz_eval(-100, -1)
        self.assertEqual(v, 0)

    def test_eval_7(self):
        v = collatz_eval(50000, 100000)
        self.assertEqual(v, 351)

    def test_eval_8(self):
        v = collatz_eval(50000, 75000)
        self.assertEqual(v, 340)

    def test_eval_9(self):
        v = collatz_eval(50000, 100500)
        self.assertEqual(v, 351)

    def test_eval_10(self):
        v = collatz_eval(50000, 250000)
        self.assertEqual(v, 443)

    def test_eval_11(self):
        v = collatz_eval(1, -1)
        self.assertEqual(v, 0)

    def test_eval_12(self):
        v = collatz_eval(100001, 100001)
        self.assertEqual(v, 90)
    
    def test_eval_13(self):
        v = collatz_eval(99999, 200001)
        self.assertEqual(v, 383)

    def test_eval_14(self):
        v = collatz_eval(1000001, 1000001)
        self.assertEqual(v, 114)

    
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 4.3, 5.9, 0)
        self.assertEqual(w.getvalue(), "4.3 5.9 0\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, "", "", "")
        self.assertEqual(w.getvalue(), "  \n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    # Ensure empty line is handled
    def test_solve_2(self):
        r = StringIO("\n")
        w = StringIO()
        collatz_solve(r, w)

    # Ensure non-int values are handled
    def test_solve_3(self):
        r = StringIO("4.4 5.6\n")
        w = StringIO()
        collatz_solve(r, w)

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......................
----------------------------------------------------------------------
Ran 23 tests in 1.326s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......................
----------------------------------------------------------------------
Ran 23 tests in 1.326s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          59      0     32      0   100%
TestCollatz.py      88      0      0      0   100%
------------------------------------------------------------
TOTAL              147      0     32      0   100%

"""
